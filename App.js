import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, SafeAreaView } from 'react-native';

// URL : www.thecocktaildb.com/api/json/v1/1/random.php

export default function App() {
  const [cocktail, setCocktail] = useState(null);
  const [data, setData] = useState(null);

  const getCocktailRandom = async () => {
    fetch(`https://www.thecocktaildb.com/api/json/v1/1/random.php`)
      .then(response => response.json())
      .then(data => {
        setData(data.drinks[0]);
      });
  }

  useEffect(() => {
    (async () => {
      for(let i = 0; i < 10; i++) {
        getCocktailRandom();
      }
    })();
  }, []);

  const Item = ({title}) => (
    <View style={styles.item}>
        <Image style={styles.tinyLogo} source={{uri: title.strDrinkThumb}}/>
        <Text style={styles.title}>{title.strDrink}</Text>
    </View>
  );

  const renderItem = ({ item }) => (
    <Item title={item} />
  );

  return (
    <SafeAreaView style={styles.container}>
      {/* Attendre que data soit charger avant d'afficher */}
      {data && (
        <>
          <Text>{console.log( data.strDrink )}</Text>
          <FlatList
            data={data}
            renderItem={renderItem}
            // keyExtractor={ item => item.strDrink }
          />
        </>
      )}
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: StatusBar.currentHeight || 0, 
  },
  tinyLogo: {
    width: 120,
    height: 120,
    borderRadius:4
  },
  title: {
    fontSize: 18,
    marginTop:15,
    textAlign:'center',
  },
  item: {
    backgroundColor: '#',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
  },
});
